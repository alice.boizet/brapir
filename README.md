# brapir

An R package to use the Breeding API (BrAPI) for accessing BrAPI compliant plant breeding databases.  
Current support is for BrAPI version 2.1  

This package is a reimplementation of the existing package [brapir-v2](https://github.com/mverouden/brapir-v2) developed by Maikel Verouden.

# Install brapir
You can install brapir with remotes  
```remotes::install_gitlab(repo = "alice.boizet/brapir", host = "https://gitlab.cirad.fr")```

# Running tests

## Use your own brapi server
You need to specify your test server in /tests/testthat/setup.R file

## Run a local testing brapi server
You can run a local testing server with docker. It is based on https://github.com/plantbreeding/brapi-Java-TestServer/   
This server implements all brapi calls with dummy data.

Go to /tests/brapi-test-server  
```cd /tests/brapi-test-server```

Create the network brapi_net  
```docker network create -d bridge brapi_net```

Run docker containers  
```docker compose up -d ```

if you want the created database to be initialized with dummy data, you have to specify the path to the sql scripts in the application.properties:  
```spring.flyway.locations=classpath:/db/migration,classpath:[path to sql directory]```  
These sql files can be found [here](https://github.com/plantbreeding/brapi-Java-TestServer/tree/brapi-server-v2/src/main/resources/db/sql)
