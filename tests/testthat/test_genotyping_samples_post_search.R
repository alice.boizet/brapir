test_that("genotyping_samples_post_search works", {
	# load brapi server connection
	source("setup.R")
	result <- brapir::genotyping_samples_post_search(con = con)
	expect_true(result$status_code == 200 || result$status_code == 202)
})