test_that("germplasm_seedlots_post_transactions works", {
	# load brapi server connection
	source("setup.R")
	amount <- 45
	result <- brapir::germplasm_seedlots_post_transactions(con = con,
                                     amount = amount)
	expect_true(result$status_code == 200)
})