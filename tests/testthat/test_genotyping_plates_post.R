test_that("genotyping_plates_post works", {
	# load brapi server connection
	source("setup.R")
	plateName <- "test"
	programDbId <- "program1"
	studyDbId <- "study1"
	trialDbId <- "trial1"
	result <- brapir::genotyping_plates_post(con = con,
                       plateName = plateName,
                       programDbId = programDbId,
                       studyDbId = studyDbId,
                       trialDbId = trialDbId)
	expect_true(result$status_code == 200)
})