test_that("phenotyping_observations_get_observationDbId works", {
	# load brapi server connection
	source("setup.R")
	result <- brapir::phenotyping_observations_get(con)
	id <- result$data$observationDbId[1]
	res <- brapir::phenotyping_observations_get_observationDbId(con, observationDbId = id)
	expect_true(res$status_code == 200)
})