test_that("phenotyping_observationunits_post works", {
	# load brapi server connection
	source("setup.R")
	crossName <- "test"
	germplasmDbId <- "germplasm1"
	germplasmName <- "Tomatillo Fantastico"
	locationDbId <- "location_01"
	locationName <- "test"
	observationUnitName <- "Plot 1"
	programDbId <- "program1"
	programName <- "test"
	seedLotName <- "test"
	studyDbId <- "study1"
	studyName <- "test"
	treatments <- data.frame(factor = c("fertilizer"),modality = c("low fertilizer"))
	trialDbId <- "trial1"
	trialName <- "test"
	result <- brapir::phenotyping_observationunits_post(con = con,
                                  crossName = crossName,
                                  germplasmDbId = germplasmDbId,
                                  germplasmName = germplasmName,
                                  locationDbId = locationDbId,
                                  locationName = locationName,
                                  observationUnitName = observationUnitName,
                                  programDbId = programDbId,
                                  programName = programName,
                                  seedLotName = seedLotName,
                                  studyDbId = studyDbId,
                                  studyName = studyName,
                                  treatments = treatments,
                                  trialDbId = trialDbId,
                                  trialName = trialName)
	expect_true(result$status_code == 200)
})