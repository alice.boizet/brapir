test_that("core_programs_get_programDbId works", {
	# load brapi server connection
	source("setup.R")
	result <- brapir::core_programs_get(con)
	id <- result$data$programDbId[1]
	res <- brapir::core_programs_get_programDbId(con, programDbId = id)
	expect_true(res$status_code == 200)
})