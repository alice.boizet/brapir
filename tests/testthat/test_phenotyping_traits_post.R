test_that("phenotyping_traits_post works", {
	# load brapi server connection
	source("setup.R")
	alternativeAbbreviations <- c("H","PH","H1")
	synonyms <- c("Height","Plant Height","Stalk Height","Canopy Height")
	traitName <- "test"
	result <- brapir::phenotyping_traits_post(con = con,
                        alternativeAbbreviations = alternativeAbbreviations,
                        synonyms = synonyms,
                        traitName = traitName)
	expect_true(result$status_code == 200)
})