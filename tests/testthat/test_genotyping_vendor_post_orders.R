test_that("genotyping_vendor_post_orders works", {
	# load brapi server connection
	source("setup.R")
	numberOfSamples <- 180
	plates <- data.frame(clientPlateBarcode = c("6ebf3f25"),clientPlateId = c("02a8d6f0"),sampleSubmissionFormat = c("PLATE_96"))
	serviceIds <- c("e8f60f64","05bd925a","b698fb5e")
	result <- brapir::genotyping_vendor_post_orders(con = con,
                              numberOfSamples = numberOfSamples,
                              plates = plates,
                              serviceIds = serviceIds)
	expect_true(result$status_code == 200)
})