test_that("germplasm_seedlots_get works", {
	# load brapi server connection
	source("setup.R")
	result <- brapir::germplasm_seedlots_get(con)
	expect_true(result$status_code == 200)
})