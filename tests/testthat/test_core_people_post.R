test_that("core_people_post works", {
	# load brapi server connection
	source("setup.R")
	firstName <- "test"
	lastName <- "test"
	middleName <- "test"
	result <- brapir::core_people_post(con = con,
                 firstName = firstName,
                 lastName = lastName,
                 middleName = middleName)
	expect_true(result$status_code == 200)
})