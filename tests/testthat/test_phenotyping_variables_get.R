test_that("phenotyping_variables_get works", {
	# load brapi server connection
	source("setup.R")
	result <- brapir::phenotyping_variables_get(con)
	expect_true(result$status_code == 200)
})