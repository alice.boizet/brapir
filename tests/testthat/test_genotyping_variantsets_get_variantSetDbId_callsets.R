test_that("genotyping_variantsets_get_variantSetDbId_callsets works", {
	# load brapi server connection
	source("setup.R")
	result <- brapir::genotyping_variantsets_get(con)
	id <- result$data$variantSetDbId[1]
	res <- brapir::genotyping_variantsets_get_variantSetDbId_callsets(con, variantSetDbId = id)
	expect_true(res$status_code == 200)
})