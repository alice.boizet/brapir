test_that("phenotyping_observations_post_search works", {
	# load brapi server connection
	source("setup.R")
	result <- brapir::phenotyping_observations_post_search(con = con)
	expect_true(result$status_code == 200 || result$status_code == 202)
})