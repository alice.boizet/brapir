test_that("core_studytypes_get works", {
	# load brapi server connection
	source("setup.R")
	result <- brapir::core_studytypes_get(con)
	expect_true(result$status_code == 200)
})