test_that("genotyping_references_get works", {
	# load brapi server connection
	source("setup.R")
	result <- brapir::genotyping_references_get(con)
	expect_true(result$status_code == 200)
})