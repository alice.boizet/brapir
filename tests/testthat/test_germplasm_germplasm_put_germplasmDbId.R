test_that("germplasm_germplasm_put_germplasmDbId works", {
	# load brapi server connection
	source("setup.R")
	result <- brapir::germplasm_germplasm_get(con)
	id <- result$data$germplasmDbId[1]
	res <- brapir::germplasm_germplasm_put_germplasmDbId(con, germplasmDbId = id)
	expect_true(res$status_code == 200)
})