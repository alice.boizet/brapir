test_that("core_studies_post works", {
	# load brapi server connection
	source("setup.R")
	active <- T
	commonCropName <- "Maize"
	contacts <- data.frame(contactDbId = c("5f4e5509"),email = c("bob@bob.com"),instituteName = c("The BrAPI Institute"),name = c("Bob Robertson"),orcid = c("http://orcid.org/0000-0001-8640-1750"),type = c("PI"))
	dataLinks <- data.frame(dataFormat = c("Image Archive"),description = c("Raw drone images collected for this study"),fileFormat = c("application/zip"),name = c("image-archive.zip"),provenance = c("Image Processing Pipeline, built at the University of Antarctica: https://github.com/antarctica/pipeline"),scientificType = c("Environmental"),url = c("https://brapi.org/image-archive.zip"),version = c("1.0.3"))
	environmentParameters <- data.frame(description = c("the soil type was clay"),parameterName = c("soil type"),parameterPUI = c("PECO:0007155"),unit = c("pH"),unitPUI = c("PECO:0007059"),value = c("clay soil"),valuePUI = c("ENVO:00002262"))
	locationDbId <- "location_01"
	locationName <- "test"
	observationLevels <- data.frame(levelName = c("plot"),levelOrder = c("0","1","2"))
	observationVariableDbIds <- c("variable1", "variable2")
	seasons <- c("fall_2011")
	studyName <- "test"
	trialDbId <- "trial1"
	trialName <- "test"
	result <- brapir::core_studies_post(con = con,
                  active = active,
                  commonCropName = commonCropName,
                  contacts = contacts,
                  dataLinks = dataLinks,
                  environmentParameters = environmentParameters,
                  locationDbId = locationDbId,
                  locationName = locationName,
                  observationLevels = observationLevels,
                  observationVariableDbIds = observationVariableDbIds,
                  seasons = seasons,
                  studyName = studyName,
                  trialDbId = trialDbId,
                  trialName = trialName)
	expect_true(result$status_code == 200)
})