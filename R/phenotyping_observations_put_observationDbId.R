#' PUT observations/{observationDbId}
#' @param con list;  required
#' @param observationDbId string;  required
#' @param additionalInfo dataframe;  optional
#' @param collector string;  optional
#' @param externalReferences dataframe;  optional
#' @param geoCoordinates dataframe;  optional
#' @param germplasmDbId string;  optional
#' @param germplasmName string;  optional
#' @param observationTimeStamp string;  optional
#' @param observationUnitDbId string;  optional
#' @param observationUnitName string;  optional
#' @param observationVariableDbId string;  optional
#' @param observationVariableName string;  optional
#' @param season dataframe;  required
#' @param studyDbId string;  optional
#' @param uploadedBy string;  optional
#' @param value string;  optional
#'@return list 
#'
#'
#'@export
phenotyping_observations_put_observationDbId <- function(con = NULL,
                                                         observationDbId = "",
                                                         additionalInfo = list(),
                                                         collector = "",
                                                         externalReferences = data.frame(),
                                                         geoCoordinates = list(),
                                                         germplasmDbId = "",
                                                         germplasmName = "",
                                                         observationTimeStamp = "",
                                                         observationUnitDbId = "",
                                                         observationUnitName = "",
                                                         observationVariableDbId = "",
                                                         observationVariableName = "",
                                                         season = list(),
                                                         studyDbId = "",
                                                         uploadedBy = "",
                                                         value = "") {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	body <- list()
	if (length(additionalInfo) > 0) {
		body <- append(body, list(additionalInfo = jsonlite::unbox(additionalInfo)))
	}
	if (all(collector != '')) {
		body <- append(body, list(collector = jsonlite::unbox(collector)))
	}
	if (length(externalReferences) > 0) {
		body <- append(body, list(externalReferences = externalReferences))
	}
	if (length(geoCoordinates) > 0) {
		body <- append(body, list(geoCoordinates = jsonlite::unbox(geoCoordinates)))
	}
	if (all(germplasmDbId != '')) {
		body <- append(body, list(germplasmDbId = jsonlite::unbox(germplasmDbId)))
	}
	if (all(germplasmName != '')) {
		body <- append(body, list(germplasmName = jsonlite::unbox(germplasmName)))
	}
	if (all(observationTimeStamp != '')) {
		body <- append(body, list(observationTimeStamp = jsonlite::unbox(observationTimeStamp)))
	}
	if (all(observationUnitDbId != '')) {
		body <- append(body, list(observationUnitDbId = jsonlite::unbox(observationUnitDbId)))
	}
	if (all(observationUnitName != '')) {
		body <- append(body, list(observationUnitName = jsonlite::unbox(observationUnitName)))
	}
	if (all(observationVariableDbId != '')) {
		body <- append(body, list(observationVariableDbId = jsonlite::unbox(observationVariableDbId)))
	}
	if (all(observationVariableName != '')) {
		body <- append(body, list(observationVariableName = jsonlite::unbox(observationVariableName)))
	}
	if (length(season) > 0) {
		body <- append(body, list(season = jsonlite::unbox(season)))
	}
	if (all(studyDbId != '')) {
		body <- append(body, list(studyDbId = jsonlite::unbox(studyDbId)))
	}
	if (all(uploadedBy != '')) {
		body <- append(body, list(uploadedBy = jsonlite::unbox(uploadedBy)))
	}
	if (all(value != '')) {
		body <- append(body, list(value = jsonlite::unbox(value)))
	}
	if (length(body) == 0) {
		body <- "{}"
	} else {
		body <- jsonlite::toJSON(body)
	}
	## Obtain the call url
	callPath <- paste0("observations/",observationDbId)
	callurl <- brapi_endpoint_URL(con = con, callPath = callPath)
	try({
		## Make the call and receive the response
		resp <- httr::PUT(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			if ("data" %in% names(res$result)) {
				out$data <- res$result$data
			} else {
				out$data <- res$result
			}
		} else {
			message(paste0("The PUT observations/{observationDbId} call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "phenotyping_observations_put_observationDbId")
	return(out)
}