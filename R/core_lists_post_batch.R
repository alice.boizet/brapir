#' POST lists (for a single element)
#' @param con list;  required
#' @param additionalInfo dataframe;  optional
#' @param data array;  optional
#' @param dateCreated string;  optional
#' @param dateModified string;  optional
#' @param externalReferences dataframe;  optional
#' @param listDescription string;  optional
#' @param listName string;  optional
#' @param listOwnerName string;  optional
#' @param listOwnerPersonDbId string;  optional
#' @param listSize integer;  optional
#' @param listSource string;  optional
#' @param listType string;  optional
#'@return list 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' core_lists_post_batch(con = con, data = dataframe) 
#' Definition and example of each column of the dataframe: 
#' additionalInfo dataframe;  optional
#'		ex: list(a = "a", b = "b")
#' data array;  optional
#' dateCreated string;  optional
#' dateModified string;  optional
#' externalReferences dataframe;  optional
#' listDescription string;  optional
#' listName string;  optional
#' listOwnerName string;  optional
#' listOwnerPersonDbId string;  optional
#' listSize integer;  optional
#' listSource string;  optional
#' }
#'
#'@export
core_lists_post_batch <- function(con = NULL, data = NULL) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	if (is(data, "list")) {
		body <- data
	} else {  #convert dataframe to list()
		body <- apply(data,1,function(a){
		b <- list()
		if ("additionalInfo" %in% names(data) ) {
			b[["additionalInfo"]] <-jsonlite::unbox(as.object(a["additionalInfo"]))
		}
		if ("data" %in% names(data) ) {
			b[["data"]] <-a["data"]
		}
		if ("dateCreated" %in% names(data) ) {
			b[["dateCreated"]] <-jsonlite::unbox(as.integer(a["dateCreated"]))
		}
		if ("dateModified" %in% names(data) ) {
			b[["dateModified"]] <-jsonlite::unbox(as.integer(a["dateModified"]))
		}
		if ("externalReferences" %in% names(data) ) {
			b[["externalReferences"]] <-a["externalReferences"]
		}
		if ("listDescription" %in% names(data) ) {
			b[["listDescription"]] <-jsonlite::unbox(as.integer(a["listDescription"]))
		}
		if ("listName" %in% names(data) ) {
			b[["listName"]] <-jsonlite::unbox(as.integer(a["listName"]))
		}
		if ("listOwnerName" %in% names(data) ) {
			b[["listOwnerName"]] <-jsonlite::unbox(as.integer(a["listOwnerName"]))
		}
		if ("listOwnerPersonDbId" %in% names(data) ) {
			b[["listOwnerPersonDbId"]] <-jsonlite::unbox(as.integer(a["listOwnerPersonDbId"]))
		}
		if ("listSize" %in% names(data) ) {
			b[["listSize"]] <-jsonlite::unbox(as.character(a["listSize"]))
		}
		if ("listSource" %in% names(data) ) {
			b[["listSource"]] <-jsonlite::unbox(as.integer(a["listSource"]))
		}
		if ("listType" %in% names(data) ) {
			b[["listType"]] <-jsonlite::unbox(as.integer(a["listType"]))
		}
		return(b)
		})
	}
	body <- jsonlite::toJSON(body)
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "lists")

	try({
		## Make the call and receive the response
		resp <- httr::POST(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			out$data <-res$result$data
		} else {
			message(paste0("The POST lists call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "core_lists_post_batch")
	return(out)
}