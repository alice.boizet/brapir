#' PUT trials/{trialDbId}
#' @param con list;  required
#' @param trialDbId string;  required
#' @param active boolean;  optional
#' @param additionalInfo dataframe;  optional
#' @param commonCropName string;  optional
#' @param contacts dataframe;  optional
#' @param datasetAuthorships dataframe;  optional
#' @param documentationURL string;  optional
#' @param endDate string;  optional
#' @param externalReferences dataframe;  optional
#' @param programDbId string;  optional
#' @param programName string;  optional
#' @param publications dataframe;  optional
#' @param startDate string;  optional
#' @param trialDescription string;  optional
#' @param trialName string;  optional
#' @param trialPUI string;  optional
#'@return list 
#'
#'
#'@export
core_trials_put_trialDbId <- function(con = NULL,
                                      trialDbId = "",
                                      active = NA,
                                      additionalInfo = list(),
                                      commonCropName = "",
                                      contacts = data.frame(),
                                      datasetAuthorships = data.frame(),
                                      documentationURL = "",
                                      endDate = "",
                                      externalReferences = data.frame(),
                                      programDbId = "",
                                      programName = "",
                                      publications = data.frame(),
                                      startDate = "",
                                      trialDescription = "",
                                      trialName = "",
                                      trialPUI = "") {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	body <- list()
	if (!is.na(active)) {
		body <- append(body, list(active = jsonlite::unbox(as.logical(active))))
	}
	if (length(additionalInfo) > 0) {
		body <- append(body, list(additionalInfo = jsonlite::unbox(additionalInfo)))
	}
	if (all(commonCropName != '')) {
		body <- append(body, list(commonCropName = jsonlite::unbox(commonCropName)))
	}
	if (length(contacts) > 0) {
		body <- append(body, list(contacts = contacts))
	}
	if (length(datasetAuthorships) > 0) {
		body <- append(body, list(datasetAuthorships = datasetAuthorships))
	}
	if (all(documentationURL != '')) {
		body <- append(body, list(documentationURL = jsonlite::unbox(documentationURL)))
	}
	if (all(endDate != '')) {
		body <- append(body, list(endDate = jsonlite::unbox(endDate)))
	}
	if (length(externalReferences) > 0) {
		body <- append(body, list(externalReferences = externalReferences))
	}
	if (all(programDbId != '')) {
		body <- append(body, list(programDbId = jsonlite::unbox(programDbId)))
	}
	if (all(programName != '')) {
		body <- append(body, list(programName = jsonlite::unbox(programName)))
	}
	if (length(publications) > 0) {
		body <- append(body, list(publications = publications))
	}
	if (all(startDate != '')) {
		body <- append(body, list(startDate = jsonlite::unbox(startDate)))
	}
	if (all(trialDescription != '')) {
		body <- append(body, list(trialDescription = jsonlite::unbox(trialDescription)))
	}
	if (all(trialName != '')) {
		body <- append(body, list(trialName = jsonlite::unbox(trialName)))
	}
	if (all(trialPUI != '')) {
		body <- append(body, list(trialPUI = jsonlite::unbox(trialPUI)))
	}
	if (length(body) == 0) {
		body <- "{}"
	} else {
		body <- jsonlite::toJSON(body)
	}
	## Obtain the call url
	callPath <- paste0("trials/",trialDbId)
	callurl <- brapi_endpoint_URL(con = con, callPath = callPath)
	try({
		## Make the call and receive the response
		resp <- httr::PUT(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			if ("data" %in% names(res$result)) {
				out$data <- res$result$data
			} else {
				out$data <- res$result
			}
		} else {
			message(paste0("The PUT trials/{trialDbId} call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "core_trials_put_trialDbId")
	return(out)
}