#' POST search/samples
#' @param con list;  required
#' @param commonCropNames array;  optional
#' @param externalReferenceIDs array;  optional
#' @param externalReferenceIds array;  optional
#' @param externalReferenceSources array;  optional
#' @param germplasmDbIds array;  optional
#' @param germplasmNames array;  optional
#' @param observationUnitDbIds array;  optional
#' @param page integer;  optional
#' @param pageSize integer;  optional
#' @param plateDbIds array;  optional
#' @param plateNames array;  optional
#' @param programDbIds array;  optional
#' @param programNames array;  optional
#' @param sampleDbIds array;  optional
#' @param sampleGroupDbIds array;  optional
#' @param sampleNames array;  optional
#' @param studyDbIds array;  optional
#' @param studyNames array;  optional
#' @param trialDbIds array;  optional
#' @param trialNames array;  optional
#'@return list 
#'
#'
#'@export
genotyping_samples_post_search <- function(con = NULL,
                                           commonCropNames = c(),
                                           externalReferenceIDs = c(),
                                           externalReferenceIds = c(),
                                           externalReferenceSources = c(),
                                           germplasmDbIds = c(),
                                           germplasmNames = c(),
                                           observationUnitDbIds = c(),
                                           page = 0,
                                           pageSize = 1000,
                                           plateDbIds = c(),
                                           plateNames = c(),
                                           programDbIds = c(),
                                           programNames = c(),
                                           sampleDbIds = c(),
                                           sampleGroupDbIds = c(),
                                           sampleNames = c(),
                                           studyDbIds = c(),
                                           studyNames = c(),
                                           trialDbIds = c(),
                                           trialNames = c()) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	body <- list()
	if (length(commonCropNames) > 0) {
		body <- append(body, list(commonCropNames = commonCropNames))
	}
	if (length(externalReferenceIDs) > 0) {
		body <- append(body, list(externalReferenceIDs = externalReferenceIDs))
	}
	if (length(externalReferenceIds) > 0) {
		body <- append(body, list(externalReferenceIds = externalReferenceIds))
	}
	if (length(externalReferenceSources) > 0) {
		body <- append(body, list(externalReferenceSources = externalReferenceSources))
	}
	if (length(germplasmDbIds) > 0) {
		body <- append(body, list(germplasmDbIds = germplasmDbIds))
	}
	if (length(germplasmNames) > 0) {
		body <- append(body, list(germplasmNames = germplasmNames))
	}
	if (length(observationUnitDbIds) > 0) {
		body <- append(body, list(observationUnitDbIds = observationUnitDbIds))
	}
	if (!is.na(page)) {
		body <- append(body, list(page = jsonlite::unbox(page)))
	}
	if (!is.na(pageSize)) {
		body <- append(body, list(pageSize = jsonlite::unbox(pageSize)))
	}
	if (length(plateDbIds) > 0) {
		body <- append(body, list(plateDbIds = plateDbIds))
	}
	if (length(plateNames) > 0) {
		body <- append(body, list(plateNames = plateNames))
	}
	if (length(programDbIds) > 0) {
		body <- append(body, list(programDbIds = programDbIds))
	}
	if (length(programNames) > 0) {
		body <- append(body, list(programNames = programNames))
	}
	if (length(sampleDbIds) > 0) {
		body <- append(body, list(sampleDbIds = sampleDbIds))
	}
	if (length(sampleGroupDbIds) > 0) {
		body <- append(body, list(sampleGroupDbIds = sampleGroupDbIds))
	}
	if (length(sampleNames) > 0) {
		body <- append(body, list(sampleNames = sampleNames))
	}
	if (length(studyDbIds) > 0) {
		body <- append(body, list(studyDbIds = studyDbIds))
	}
	if (length(studyNames) > 0) {
		body <- append(body, list(studyNames = studyNames))
	}
	if (length(trialDbIds) > 0) {
		body <- append(body, list(trialDbIds = trialDbIds))
	}
	if (length(trialNames) > 0) {
		body <- append(body, list(trialNames = trialNames))
	}
	if (length(body) == 0) {
		body <- "{}"
	} else {
		body <- jsonlite::toJSON(body)
	}
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "search/samples")

	try({
		## Make the call and receive the response
		resp <- httr::POST(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			message(paste0("Immediate Response.", "\n"))
			if (out$metadata$pagination$totalCount > 0) {
				out$data <-res$result$data
			} else if ("searchResultsDbId" %in% names(res$result)){
				out$data <- res$result
			} else {
				out$data <- data.frame()
			}
		} else if (httr::status_code(resp) == 202) {
			message(paste0("Saved or Asynchronous Response has provided a searchResultsDbId.", "\n"))
			message(paste0("Use the GET search/samples/{searchResultsDbId} call to retrieve the paginated output.", "\n"))
			out$data <-res$result
		} else {
			message(paste0("The POST search/samples call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "genotyping_samples_post_search")
	return(out)
}