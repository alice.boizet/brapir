#' GET seedlots
#' @param con list;  required
#' @param seedLotDbId string;  optional
#' @param crossDbId string;  optional
#' @param crossName string;  optional
#' @param commonCropName string;  optional
#' @param programDbId string;  optional
#' @param germplasmDbId string;  optional
#' @param germplasmName string;  optional
#' @param externalReferenceID string;  optional
#' @param externalReferenceId string;  optional
#' @param externalReferenceSource string;  optional
#' @param page integer;  optional
#' @param pageSize integer;  optional
#'@return data.frame 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' germplasm_seedlots_get(con = con)
#' }
#'
#'@export
germplasm_seedlots_get <- function(con = NULL,
                                   seedLotDbId = NULL,
                                   crossDbId = NULL,
                                   crossName = NULL,
                                   commonCropName = NULL,
                                   programDbId = NULL,
                                   germplasmDbId = NULL,
                                   germplasmName = NULL,
                                   externalReferenceID = NULL,
                                   externalReferenceId = NULL,
                                   externalReferenceSource = NULL,
                                   page = 0,
                                   pageSize = 1000) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	queryParams <- list()
	if (!is.null(seedLotDbId) && (seedLotDbId != "")){
		queryParams[["seedLotDbId"]] = seedLotDbId
	}
	if (!is.null(crossDbId) && (crossDbId != "")){
		queryParams[["crossDbId"]] = crossDbId
	}
	if (!is.null(crossName) && (crossName != "")){
		queryParams[["crossName"]] = crossName
	}
	if (!is.null(commonCropName) && (commonCropName != "")){
		queryParams[["commonCropName"]] = commonCropName
	}
	if (!is.null(programDbId) && (programDbId != "")){
		queryParams[["programDbId"]] = programDbId
	}
	if (!is.null(germplasmDbId) && (germplasmDbId != "")){
		queryParams[["germplasmDbId"]] = germplasmDbId
	}
	if (!is.null(germplasmName) && (germplasmName != "")){
		queryParams[["germplasmName"]] = germplasmName
	}
	if (!is.null(externalReferenceID) && (externalReferenceID != "")){
		queryParams[["externalReferenceID"]] = externalReferenceID
	}
	if (!is.null(externalReferenceId) && (externalReferenceId != "")){
		queryParams[["externalReferenceId"]] = externalReferenceId
	}
	if (!is.null(externalReferenceSource) && (externalReferenceSource != "")){
		queryParams[["externalReferenceSource"]] = externalReferenceSource
	}
	if (!is.null(page) && (page != "")){
		queryParams[["page"]] = page
	}
	if (!is.null(pageSize) && (pageSize != "")){
		queryParams[["pageSize"]] = pageSize
	}
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "seedlots")

	try({
		## Make the call and receive the response
		resp <- httr::GET(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			query = queryParams
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			if (out$metadata$pagination$totalCount > 0) {
				out$data <-res$result$data
			} else if ("searchResultsDbId" %in% names(res$result)){
				out$data <- res$result
			} else {
				out$data <- data.frame()
			}
		} else {
			message(paste0("The GET seedlots call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "germplasm_seedlots_get")
	return(out)
}