#' POST methods (for a single element)
#' @param con list;  required
#' @param additionalInfo dataframe;  optional
#' @param bibliographicalReference string;  optional
#' @param description string;  optional
#' @param externalReferences dataframe;  optional
#' @param formula string;  optional
#' @param methodClass string;  optional
#' @param methodName string;  optional
#' @param methodPUI string;  optional
#' @param ontologyReference dataframe;  required
#'@return list 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' phenotyping_methods_post_batch(con = con, data = dataframe) 
#' Definition and example of each column of the dataframe: 
#' additionalInfo dataframe;  optional
#'		ex: list(a = "a", b = "b")
#' bibliographicalReference string;  optional
#' description string;  optional
#' externalReferences dataframe;  optional
#' formula string;  optional
#' methodClass string;  optional
#' methodName string;  optional
#' methodPUI string;  optional
#' }
#'
#'@export
phenotyping_methods_post_batch <- function(con = NULL, data = NULL) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	if (is(data, "list")) {
		body <- data
	} else {  #convert dataframe to list()
		body <- apply(data,1,function(a){
		b <- list()
		if ("additionalInfo" %in% names(data) ) {
			b[["additionalInfo"]] <-jsonlite::unbox(as.object(a["additionalInfo"]))
		}
		if ("bibliographicalReference" %in% names(data) ) {
			b[["bibliographicalReference"]] <-jsonlite::unbox(as.integer(a["bibliographicalReference"]))
		}
		if ("description" %in% names(data) ) {
			b[["description"]] <-jsonlite::unbox(as.integer(a["description"]))
		}
		if ("externalReferences" %in% names(data) ) {
			b[["externalReferences"]] <-a["externalReferences"]
		}
		if ("formula" %in% names(data) ) {
			b[["formula"]] <-jsonlite::unbox(as.integer(a["formula"]))
		}
		if ("methodClass" %in% names(data) ) {
			b[["methodClass"]] <-jsonlite::unbox(as.integer(a["methodClass"]))
		}
		if ("methodName" %in% names(data) ) {
			b[["methodName"]] <-jsonlite::unbox(as.integer(a["methodName"]))
		}
		if ("methodPUI" %in% names(data) ) {
			b[["methodPUI"]] <-jsonlite::unbox(as.integer(a["methodPUI"]))
		}
		if ("ontologyReference" %in% names(data) ) {
			b[["ontologyReference"]] <-jsonlite::unbox(as.object(a["ontologyReference"]))
		}
		return(b)
		})
	}
	body <- jsonlite::toJSON(body)
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "methods")

	try({
		## Make the call and receive the response
		resp <- httr::POST(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			out$data <-res$result$data
		} else {
			message(paste0("The POST methods call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "phenotyping_methods_post_batch")
	return(out)
}