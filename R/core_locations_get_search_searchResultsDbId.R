#' GET search/locations/{searchResultsDbId}
#' @param con list;  required
#' @param searchResultsDbId string;  required
#' @param page integer;  optional
#' @param pageSize integer;  optional
#'@return list 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' core_locations_get_search_searchResultsDbId(con = con)
#' }
#'
#'@export
core_locations_get_search_searchResultsDbId <- function(con = NULL,
                                                        searchResultsDbId = "",
                                                        page = 0,
                                                        pageSize = 1000) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	queryParams <- list()
	if (!is.null(page) && (page != "")){
		queryParams[["page"]] = page
	}
	if (!is.null(pageSize) && (pageSize != "")){
		queryParams[["pageSize"]] = pageSize
	}
	## Obtain the call url
	callPath <- paste0("search/locations/",searchResultsDbId)
	callurl <- brapi_endpoint_URL(con = con, callPath = callPath)
	try({
		## Make the call and receive the response
		resp <- httr::GET(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			query = queryParams
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		## Check call status
		while (httr::status_code(resp) == 202) {
			Sys.sleep(5)
			resp <- httr::GET(url = callurl,
			                  httr::add_headers(Authorization = paste("Bearer", con$token)),
			                  query = queryParams)
			status <- jsonlite::fromJSON(httr::content(x = resp,
			                             as = "text",
			                             encoding = "UTF-8"))[["metadata"]][["status"]]
			if (length(status) != 0) {
				brapir:::brapi_message(msg = paste0(status[["message"]], "\n"))
			}
		}
		if (httr::status_code(resp) == 200) {
			## Extract the content from the response object in human readable form
			cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
			## Convert the content object into a data.frame
			res <- jsonlite::fromJSON(cont, flatten = T)
			out$metadata = res$metadata
			if (out$metadata$pagination$totalCount > 0) {
				out$data <-res$result$data
			} else {
				out$data <- data.frame()
			}
		} else {
			message(paste0("The GET search/locations/{searchResultsDbId} call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "core_locations_get_search_searchResultsDbId")
	return(out)
}