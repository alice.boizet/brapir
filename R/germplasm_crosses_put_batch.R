#' PUT crosses (for a single element)
#' @param con list;  required
#' @param additionalInfo dataframe;  optional
#' @param crossAttributes dataframe;  optional
#' @param crossName string;  optional
#' @param crossType string;  optional
#' @param crossingProjectDbId string;  optional
#' @param crossingProjectName string;  optional
#' @param externalReferences dataframe;  optional
#' @param parent1 dataframe;  optional
#' @param parent2 dataframe;  optional
#' @param plannedCrossDbId string;  optional
#' @param plannedCrossName string;  optional
#' @param pollinationEvents dataframe;  optional
#' @param pollinationTimeStamp string;  optional
#'@return list 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' germplasm_crosses_put_batch(con = con, data = dataframe) 
#' Definition and example of each column of the dataframe: 
#' additionalInfo dataframe;  optional
#'		ex: list(a = "a", b = "b")
#' crossAttributes dataframe;  optional
#'		ex: df$crossAttributes = list(list(crossAttributeName = Humidity Percentage,crossAttributeValue = 45))
#' crossName string;  optional
#' crossType string;  optional
#' crossingProjectDbId string;  optional
#' crossingProjectName string;  optional
#' externalReferences dataframe;  optional
#' parent1 dataframe;  optional
#'		ex: list(a = "a", b = "b")
#' parent2 dataframe;  optional
#'		ex: list(a = "a", b = "b")
#' plannedCrossDbId string;  optional
#' plannedCrossName string;  optional
#' pollinationEvents dataframe;  optional
#'		ex: df$pollinationEvents = list(list(pollinationNumber = ,pollinationSuccessful = ,pollinationTimeStamp = ))
#' }
#'
#'@export
germplasm_crosses_put_batch <- function(con = NULL, data = NULL) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	if (is(data, "list")) {
		body <- data
	} else {  #convert dataframe to list()
		body <- apply(data,1,function(a){
		b <- list()
		if ("additionalInfo" %in% names(data) ) {
			b[["additionalInfo"]] <-jsonlite::unbox(as.object(a["additionalInfo"]))
		}
		if ("crossAttributes" %in% names(data) ) {
			b[["crossAttributes"]] <-a["crossAttributes"]
		}
		if ("crossName" %in% names(data) ) {
			b[["crossName"]] <-jsonlite::unbox(as.integer(a["crossName"]))
		}
		if ("crossType" %in% names(data) ) {
			b[["crossType"]] <-jsonlite::unbox(as.integer(a["crossType"]))
		}
		if ("crossingProjectDbId" %in% names(data) ) {
			b[["crossingProjectDbId"]] <-jsonlite::unbox(as.integer(a["crossingProjectDbId"]))
		}
		if ("crossingProjectName" %in% names(data) ) {
			b[["crossingProjectName"]] <-jsonlite::unbox(as.integer(a["crossingProjectName"]))
		}
		if ("externalReferences" %in% names(data) ) {
			b[["externalReferences"]] <-a["externalReferences"]
		}
		if ("parent1" %in% names(data) ) {
			b[["parent1"]] <-jsonlite::unbox(as.object(a["parent1"]))
		}
		if ("parent2" %in% names(data) ) {
			b[["parent2"]] <-jsonlite::unbox(as.object(a["parent2"]))
		}
		if ("plannedCrossDbId" %in% names(data) ) {
			b[["plannedCrossDbId"]] <-jsonlite::unbox(as.integer(a["plannedCrossDbId"]))
		}
		if ("plannedCrossName" %in% names(data) ) {
			b[["plannedCrossName"]] <-jsonlite::unbox(as.integer(a["plannedCrossName"]))
		}
		if ("pollinationEvents" %in% names(data) ) {
			b[["pollinationEvents"]] <-a["pollinationEvents"]
		}
		if ("pollinationTimeStamp" %in% names(data) ) {
			b[["pollinationTimeStamp"]] <-jsonlite::unbox(as.integer(a["pollinationTimeStamp"]))
		}
		return(b)
		})
	}
	body <- jsonlite::toJSON(body)
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "crosses")

	try({
		## Make the call and receive the response
		resp <- httr::PUT(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
		} else {
			message(paste0("The PUT crosses call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "germplasm_crosses_put_batch")
	return(out)
}