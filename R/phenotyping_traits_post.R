#' POST traits for several elements
#' @param con list;  required
#' @param additionalInfo dataframe;  optional
#' @param alternativeAbbreviations array;  optional
#' @param attribute string;  optional
#' @param attributePUI string;  optional
#' @param entity string;  optional
#' @param entityPUI string;  optional
#' @param externalReferences dataframe;  optional
#' @param mainAbbreviation string;  optional
#' @param ontologyReference dataframe;  required
#' @param status string;  optional
#' @param synonyms array;  optional
#' @param traitClass string;  optional
#' @param traitDescription string;  optional
#' @param traitName string;  optional
#' @param traitPUI string;  optional
#'@return list 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' additionalInfo <- ""
#' alternativeAbbreviations <- c("H","PH","H1")
#' attribute <- ""
#' attributePUI <- ""
#' entity <- ""
#' entityPUI <- ""
#' externalReferences <- ""
#' mainAbbreviation <- ""
#' ontologyReference <- ""
#' status <- ""
#' synonyms <- c("Height","Plant Height","Stalk Height","Canopy Height")
#' traitClass <- ""
#' traitDescription <- ""
#' traitName <- ""
#' phenotyping_traits_post(additionalInfo = additionalInfo,
#'                         alternativeAbbreviations = alternativeAbbreviations,
#'                         attribute = attribute,
#'                         attributePUI = attributePUI,
#'                         entity = entity,
#'                         entityPUI = entityPUI,
#'                         externalReferences = externalReferences,
#'                         mainAbbreviation = mainAbbreviation,
#'                         ontologyReference = ontologyReference,
#'                         status = status,
#'                         synonyms = synonyms,
#'                         traitClass = traitClass,
#'                         traitDescription = traitDescription,
#'                         traitName = traitName)
#' }
#'
#'@export
phenotyping_traits_post <- function(con = NULL,
                                    additionalInfo = list(),
                                    alternativeAbbreviations = c(),
                                    attribute = "",
                                    attributePUI = "",
                                    entity = "",
                                    entityPUI = "",
                                    externalReferences = data.frame(),
                                    mainAbbreviation = "",
                                    ontologyReference = list(),
                                    status = "",
                                    synonyms = c(),
                                    traitClass = "",
                                    traitDescription = "",
                                    traitName = "",
                                    traitPUI = "") {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	body <- list()
	if (length(additionalInfo) > 0) {
		body <- append(body, list(additionalInfo = jsonlite::unbox(additionalInfo)))
	}
	if (length(alternativeAbbreviations) > 0) {
		body <- append(body, list(alternativeAbbreviations = alternativeAbbreviations))
	}
	if (all(attribute != '')) {
		body <- append(body, list(attribute = jsonlite::unbox(attribute)))
	}
	if (all(attributePUI != '')) {
		body <- append(body, list(attributePUI = jsonlite::unbox(attributePUI)))
	}
	if (all(entity != '')) {
		body <- append(body, list(entity = jsonlite::unbox(entity)))
	}
	if (all(entityPUI != '')) {
		body <- append(body, list(entityPUI = jsonlite::unbox(entityPUI)))
	}
	if (length(externalReferences) > 0) {
		body <- append(body, list(externalReferences = externalReferences))
	}
	if (all(mainAbbreviation != '')) {
		body <- append(body, list(mainAbbreviation = jsonlite::unbox(mainAbbreviation)))
	}
	if (length(ontologyReference) > 0) {
		body <- append(body, list(ontologyReference = jsonlite::unbox(ontologyReference)))
	}
	if (all(status != '')) {
		body <- append(body, list(status = jsonlite::unbox(status)))
	}
	if (length(synonyms) > 0) {
		body <- append(body, list(synonyms = synonyms))
	}
	if (all(traitClass != '')) {
		body <- append(body, list(traitClass = jsonlite::unbox(traitClass)))
	}
	if (all(traitDescription != '')) {
		body <- append(body, list(traitDescription = jsonlite::unbox(traitDescription)))
	}
	if (all(traitName != '')) {
		body <- append(body, list(traitName = jsonlite::unbox(traitName)))
	}
	if (all(traitPUI != '')) {
		body <- append(body, list(traitPUI = jsonlite::unbox(traitPUI)))
	}
	# this service expects a list of json
	body <- list(body)
	if (length(body) == 0) {
		body <- "{}"
	} else {
		body <- jsonlite::toJSON(body)
	}
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "traits")

	try({
		## Make the call and receive the response
		resp <- httr::POST(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			out$data <-res$result$data
		} else {
			message(paste0("The POST traits call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "phenotyping_traits_post")
	return(out)
}