#' Helper function for checking connection object arguments
#'
#' @author Maikel Verouden
#'
#' @noRd
#' @keywords internal
brapi_checkConArgs <- function(secure = FALSE,
                               db = "127.0.0.1",
                               port = 80,
                               apipath = NULL,
                               multicrop = FALSE,
                               commoncropname = "",
                               token = "",
                               granttype = "password",
                               clientid = "brapirv2") {
  ## Check function arguments
  if (!is.logical(secure)) {
    stop("The secure argument can only be a logical (TRUE or FALSE) value.")
  }
  if (!is.character(db)) {
    stop("The db argument can only be a character string.")
  }
  if (!is.numeric(port) | port < 1) {
    stop("The port argument can only be a numeric value > 0.")
  }
  if (!xor(is.null(apipath), is.character(apipath))) {
    stop("The apipath argument can only be NULL or a character string.")
  }
  if (!is.character(commoncropname)) {
    stop("The commoncropname argument can only be a character string.")
  }
  if (!is.logical(multicrop)) {
    stop("The multicrop argument can only be a logical value (TRUE or FALSE).")
  }
  if (!is.character(token)) {
    stop("The token argument can only be a character string.")
  }
  if (!is.character(granttype)) {
    stop("The granttype argument can only be a character string.")
  }
  if (!is.character(clientid)) {
    stop("The clientid argument can only be a character string.")
  }
  return(invisible(TRUE))
}
