#' PUT germplasm/{germplasmDbId}
#' @param con list;  required
#' @param germplasmDbId string;  required
#' @param accessionNumber string;  optional
#' @param acquisitionDate string;  optional
#' @param additionalInfo dataframe;  optional
#' @param biologicalStatusOfAccessionCode string;  optional
#' @param biologicalStatusOfAccessionDescription string;  optional
#' @param breedingMethodDbId string;  optional
#' @param breedingMethodName string;  optional
#' @param collection string;  optional
#' @param commonCropName string;  optional
#' @param countryOfOriginCode string;  optional
#' @param defaultDisplayName string;  optional
#' @param documentationURL string;  optional
#' @param donors dataframe;  optional
#' @param externalReferences dataframe;  optional
#' @param genus string;  optional
#' @param germplasmName string;  optional
#' @param germplasmOrigin dataframe;  optional
#' @param germplasmPUI string;  optional
#' @param germplasmPreprocessing string;  optional
#' @param instituteCode string;  optional
#' @param instituteName string;  optional
#' @param pedigree string;  optional
#' @param seedSource string;  optional
#' @param seedSourceDescription string;  optional
#' @param species string;  optional
#' @param speciesAuthority string;  optional
#' @param storageTypes dataframe;  optional
#' @param subtaxa string;  optional
#' @param subtaxaAuthority string;  optional
#' @param synonyms dataframe;  optional
#' @param taxonIds dataframe;  optional
#'@return list 
#'
#'
#'@export
germplasm_germplasm_put_germplasmDbId <- function(con = NULL,
                                                  germplasmDbId = "",
                                                  accessionNumber = "",
                                                  acquisitionDate = "",
                                                  additionalInfo = list(),
                                                  biologicalStatusOfAccessionCode = "",
                                                  biologicalStatusOfAccessionDescription = "",
                                                  breedingMethodDbId = "",
                                                  breedingMethodName = "",
                                                  collection = "",
                                                  commonCropName = "",
                                                  countryOfOriginCode = "",
                                                  defaultDisplayName = "",
                                                  documentationURL = "",
                                                  donors = data.frame(),
                                                  externalReferences = data.frame(),
                                                  genus = "",
                                                  germplasmName = "",
                                                  germplasmOrigin = data.frame(),
                                                  germplasmPUI = "",
                                                  germplasmPreprocessing = "",
                                                  instituteCode = "",
                                                  instituteName = "",
                                                  pedigree = "",
                                                  seedSource = "",
                                                  seedSourceDescription = "",
                                                  species = "",
                                                  speciesAuthority = "",
                                                  storageTypes = data.frame(),
                                                  subtaxa = "",
                                                  subtaxaAuthority = "",
                                                  synonyms = data.frame(),
                                                  taxonIds = data.frame()) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	body <- list()
	if (all(accessionNumber != '')) {
		body <- append(body, list(accessionNumber = jsonlite::unbox(accessionNumber)))
	}
	if (all(acquisitionDate != '')) {
		body <- append(body, list(acquisitionDate = jsonlite::unbox(acquisitionDate)))
	}
	if (length(additionalInfo) > 0) {
		body <- append(body, list(additionalInfo = jsonlite::unbox(additionalInfo)))
	}
	if (all(biologicalStatusOfAccessionCode != '')) {
		body <- append(body, list(biologicalStatusOfAccessionCode = jsonlite::unbox(biologicalStatusOfAccessionCode)))
	}
	if (all(biologicalStatusOfAccessionDescription != '')) {
		body <- append(body, list(biologicalStatusOfAccessionDescription = jsonlite::unbox(biologicalStatusOfAccessionDescription)))
	}
	if (all(breedingMethodDbId != '')) {
		body <- append(body, list(breedingMethodDbId = jsonlite::unbox(breedingMethodDbId)))
	}
	if (all(breedingMethodName != '')) {
		body <- append(body, list(breedingMethodName = jsonlite::unbox(breedingMethodName)))
	}
	if (all(collection != '')) {
		body <- append(body, list(collection = jsonlite::unbox(collection)))
	}
	if (all(commonCropName != '')) {
		body <- append(body, list(commonCropName = jsonlite::unbox(commonCropName)))
	}
	if (all(countryOfOriginCode != '')) {
		body <- append(body, list(countryOfOriginCode = jsonlite::unbox(countryOfOriginCode)))
	}
	if (all(defaultDisplayName != '')) {
		body <- append(body, list(defaultDisplayName = jsonlite::unbox(defaultDisplayName)))
	}
	if (all(documentationURL != '')) {
		body <- append(body, list(documentationURL = jsonlite::unbox(documentationURL)))
	}
	if (length(donors) > 0) {
		body <- append(body, list(donors = donors))
	}
	if (length(externalReferences) > 0) {
		body <- append(body, list(externalReferences = externalReferences))
	}
	if (all(genus != '')) {
		body <- append(body, list(genus = jsonlite::unbox(genus)))
	}
	if (all(germplasmName != '')) {
		body <- append(body, list(germplasmName = jsonlite::unbox(germplasmName)))
	}
	if (length(germplasmOrigin) > 0) {
		body <- append(body, list(germplasmOrigin = germplasmOrigin))
	}
	if (all(germplasmPUI != '')) {
		body <- append(body, list(germplasmPUI = jsonlite::unbox(germplasmPUI)))
	}
	if (all(germplasmPreprocessing != '')) {
		body <- append(body, list(germplasmPreprocessing = jsonlite::unbox(germplasmPreprocessing)))
	}
	if (all(instituteCode != '')) {
		body <- append(body, list(instituteCode = jsonlite::unbox(instituteCode)))
	}
	if (all(instituteName != '')) {
		body <- append(body, list(instituteName = jsonlite::unbox(instituteName)))
	}
	if (all(pedigree != '')) {
		body <- append(body, list(pedigree = jsonlite::unbox(pedigree)))
	}
	if (all(seedSource != '')) {
		body <- append(body, list(seedSource = jsonlite::unbox(seedSource)))
	}
	if (all(seedSourceDescription != '')) {
		body <- append(body, list(seedSourceDescription = jsonlite::unbox(seedSourceDescription)))
	}
	if (all(species != '')) {
		body <- append(body, list(species = jsonlite::unbox(species)))
	}
	if (all(speciesAuthority != '')) {
		body <- append(body, list(speciesAuthority = jsonlite::unbox(speciesAuthority)))
	}
	if (length(storageTypes) > 0) {
		body <- append(body, list(storageTypes = storageTypes))
	}
	if (all(subtaxa != '')) {
		body <- append(body, list(subtaxa = jsonlite::unbox(subtaxa)))
	}
	if (all(subtaxaAuthority != '')) {
		body <- append(body, list(subtaxaAuthority = jsonlite::unbox(subtaxaAuthority)))
	}
	if (length(synonyms) > 0) {
		body <- append(body, list(synonyms = synonyms))
	}
	if (length(taxonIds) > 0) {
		body <- append(body, list(taxonIds = taxonIds))
	}
	if (length(body) == 0) {
		body <- "{}"
	} else {
		body <- jsonlite::toJSON(body)
	}
	## Obtain the call url
	callPath <- paste0("germplasm/",germplasmDbId)
	callurl <- brapi_endpoint_URL(con = con, callPath = callPath)
	try({
		## Make the call and receive the response
		resp <- httr::PUT(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			if ("data" %in% names(res$result)) {
				out$data <- res$result$data
			} else {
				out$data <- res$result
			}
		} else {
			message(paste0("The PUT germplasm/{germplasmDbId} call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "germplasm_germplasm_put_germplasmDbId")
	return(out)
}