#' PUT pedigree for several elements
#' @param con list;  required
#' @param additionalInfo dataframe;  optional
#' @param breedingMethodDbId string;  optional
#' @param breedingMethodName string;  optional
#' @param crossingProjectDbId string;  optional
#' @param crossingYear integer;  optional
#' @param defaultDisplayName string;  optional
#' @param externalReferences dataframe;  optional
#' @param familyCode string;  optional
#' @param germplasmDbId string;  optional
#' @param germplasmName string;  optional
#' @param germplasmPUI string;  optional
#' @param parents dataframe;  optional
#' @param pedigreeString string;  optional
#' @param progeny dataframe;  optional
#' @param siblings dataframe;  optional
#'@return list 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' additionalInfo <- ""
#' breedingMethodDbId <- ""
#' breedingMethodName <- ""
#' crossingProjectDbId <- ""
#' crossingYear <- 2005
#' defaultDisplayName <- ""
#' externalReferences <- ""
#' familyCode <- ""
#' germplasmDbId <- ""
#' germplasmName <- ""
#' germplasmPUI <- ""
#' parents <- data.frame(germplasmDbId = c("germplasm1"),germplasmName = c("Tomatillo Fantastico"),parentType = c("MALE","FEMALE"))
#' pedigreeString <- ""
#' progeny <- data.frame(germplasmDbId = c("germplasm1"),germplasmName = c("Tomatillo Fantastico"),parentType = c("FEMALE","FEMALE","FEMALE"))
#' germplasm_pedigree_put(additionalInfo = additionalInfo,
#'                        breedingMethodDbId = breedingMethodDbId,
#'                        breedingMethodName = breedingMethodName,
#'                        crossingProjectDbId = crossingProjectDbId,
#'                        crossingYear = crossingYear,
#'                        defaultDisplayName = defaultDisplayName,
#'                        externalReferences = externalReferences,
#'                        familyCode = familyCode,
#'                        germplasmDbId = germplasmDbId,
#'                        germplasmName = germplasmName,
#'                        germplasmPUI = germplasmPUI,
#'                        parents = parents,
#'                        pedigreeString = pedigreeString,
#'                        progeny = progeny)
#' }
#'
#'@export
germplasm_pedigree_put <- function(con = NULL,
                                   additionalInfo = list(),
                                   breedingMethodDbId = "",
                                   breedingMethodName = "",
                                   crossingProjectDbId = "",
                                   crossingYear = as.integer(NA),
                                   defaultDisplayName = "",
                                   externalReferences = data.frame(),
                                   familyCode = "",
                                   germplasmDbId = "",
                                   germplasmName = "",
                                   germplasmPUI = "",
                                   parents = data.frame(),
                                   pedigreeString = "",
                                   progeny = data.frame(),
                                   siblings = data.frame()) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	body <- list()
	if (length(additionalInfo) > 0) {
		body <- append(body, list(additionalInfo = jsonlite::unbox(additionalInfo)))
	}
	if (all(breedingMethodDbId != '')) {
		body <- append(body, list(breedingMethodDbId = jsonlite::unbox(breedingMethodDbId)))
	}
	if (all(breedingMethodName != '')) {
		body <- append(body, list(breedingMethodName = jsonlite::unbox(breedingMethodName)))
	}
	if (all(crossingProjectDbId != '')) {
		body <- append(body, list(crossingProjectDbId = jsonlite::unbox(crossingProjectDbId)))
	}
	if (!is.na(crossingYear)) {
		body <- append(body, list(crossingYear = jsonlite::unbox(crossingYear)))
	}
	if (all(defaultDisplayName != '')) {
		body <- append(body, list(defaultDisplayName = jsonlite::unbox(defaultDisplayName)))
	}
	if (length(externalReferences) > 0) {
		body <- append(body, list(externalReferences = externalReferences))
	}
	if (all(familyCode != '')) {
		body <- append(body, list(familyCode = jsonlite::unbox(familyCode)))
	}
	if (all(germplasmDbId != '')) {
		body <- append(body, list(germplasmDbId = jsonlite::unbox(germplasmDbId)))
	}
	if (all(germplasmName != '')) {
		body <- append(body, list(germplasmName = jsonlite::unbox(germplasmName)))
	}
	if (all(germplasmPUI != '')) {
		body <- append(body, list(germplasmPUI = jsonlite::unbox(germplasmPUI)))
	}
	if (length(parents) > 0) {
		body <- append(body, list(parents = parents))
	}
	if (all(pedigreeString != '')) {
		body <- append(body, list(pedigreeString = jsonlite::unbox(pedigreeString)))
	}
	if (length(progeny) > 0) {
		body <- append(body, list(progeny = progeny))
	}
	if (length(siblings) > 0) {
		body <- append(body, list(siblings = siblings))
	}
	if (length(body) == 0) {
		body <- "{}"
	} else {
		body <- jsonlite::toJSON(body)
	}
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "pedigree")

	try({
		## Make the call and receive the response
		resp <- httr::PUT(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
		} else {
			message(paste0("The PUT pedigree call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "germplasm_pedigree_put")
	return(out)
}