#' POST seedlots for several elements
#' @param con list;  required
#' @param additionalInfo dataframe;  optional
#' @param amount integer;  optional
#' @param contentMixture dataframe;  optional
#' @param createdDate string;  optional
#' @param externalReferences dataframe;  optional
#' @param lastUpdated string;  optional
#' @param locationDbId string;  optional
#' @param locationName string;  optional
#' @param programDbId string;  optional
#' @param programName string;  optional
#' @param seedLotDescription string;  optional
#' @param seedLotName string;  optional
#' @param sourceCollection string;  optional
#' @param storageLocation string;  optional
#' @param units string;  optional
#'@return list 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' additionalInfo <- ""
#' amount <- 561
#' contentMixture <- data.frame(crossDbId = c("d105fd6f"),crossName = c("my_Crosses_2018_01"),germplasmDbId = c("germplasm1"),germplasmName = c("Tomatillo Fantastico"))
#' createdDate <- ""
#' externalReferences <- ""
#' lastUpdated <- ""
#' locationDbId <- ""
#' locationName <- ""
#' programDbId <- ""
#' programName <- ""
#' seedLotDescription <- ""
#' seedLotName <- ""
#' sourceCollection <- ""
#' storageLocation <- ""
#' germplasm_seedlots_post(additionalInfo = additionalInfo,
#'                         amount = amount,
#'                         contentMixture = contentMixture,
#'                         createdDate = createdDate,
#'                         externalReferences = externalReferences,
#'                         lastUpdated = lastUpdated,
#'                         locationDbId = locationDbId,
#'                         locationName = locationName,
#'                         programDbId = programDbId,
#'                         programName = programName,
#'                         seedLotDescription = seedLotDescription,
#'                         seedLotName = seedLotName,
#'                         sourceCollection = sourceCollection,
#'                         storageLocation = storageLocation)
#' }
#'
#'@export
germplasm_seedlots_post <- function(con = NULL,
                                    additionalInfo = list(),
                                    amount = as.integer(NA),
                                    contentMixture = data.frame(),
                                    createdDate = "",
                                    externalReferences = data.frame(),
                                    lastUpdated = "",
                                    locationDbId = "",
                                    locationName = "",
                                    programDbId = "",
                                    programName = "",
                                    seedLotDescription = "",
                                    seedLotName = "",
                                    sourceCollection = "",
                                    storageLocation = "",
                                    units = "") {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	body <- list()
	if (length(additionalInfo) > 0) {
		body <- append(body, list(additionalInfo = jsonlite::unbox(additionalInfo)))
	}
	if (!is.na(amount)) {
		body <- append(body, list(amount = jsonlite::unbox(amount)))
	}
	if (length(contentMixture) > 0) {
		body <- append(body, list(contentMixture = contentMixture))
	}
	if (all(createdDate != '')) {
		body <- append(body, list(createdDate = jsonlite::unbox(createdDate)))
	}
	if (length(externalReferences) > 0) {
		body <- append(body, list(externalReferences = externalReferences))
	}
	if (all(lastUpdated != '')) {
		body <- append(body, list(lastUpdated = jsonlite::unbox(lastUpdated)))
	}
	if (all(locationDbId != '')) {
		body <- append(body, list(locationDbId = jsonlite::unbox(locationDbId)))
	}
	if (all(locationName != '')) {
		body <- append(body, list(locationName = jsonlite::unbox(locationName)))
	}
	if (all(programDbId != '')) {
		body <- append(body, list(programDbId = jsonlite::unbox(programDbId)))
	}
	if (all(programName != '')) {
		body <- append(body, list(programName = jsonlite::unbox(programName)))
	}
	if (all(seedLotDescription != '')) {
		body <- append(body, list(seedLotDescription = jsonlite::unbox(seedLotDescription)))
	}
	if (all(seedLotName != '')) {
		body <- append(body, list(seedLotName = jsonlite::unbox(seedLotName)))
	}
	if (all(sourceCollection != '')) {
		body <- append(body, list(sourceCollection = jsonlite::unbox(sourceCollection)))
	}
	if (all(storageLocation != '')) {
		body <- append(body, list(storageLocation = jsonlite::unbox(storageLocation)))
	}
	if (all(units != '')) {
		body <- append(body, list(units = jsonlite::unbox(units)))
	}
	# this service expects a list of json
	body <- list(body)
	if (length(body) == 0) {
		body <- "{}"
	} else {
		body <- jsonlite::toJSON(body)
	}
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "seedlots")

	try({
		## Make the call and receive the response
		resp <- httr::POST(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			out$data <-res$result$data
		} else {
			message(paste0("The POST seedlots call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "germplasm_seedlots_post")
	return(out)
}