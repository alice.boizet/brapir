#' GET people
#' @param con list;  required
#' @param firstName string;  optional
#' @param lastName string;  optional
#' @param personDbId string;  optional
#' @param userID string;  optional
#' @param commonCropName string;  optional
#' @param programDbId string;  optional
#' @param externalReferenceID string;  optional
#' @param externalReferenceId string;  optional
#' @param externalReferenceSource string;  optional
#' @param page integer;  optional
#' @param pageSize integer;  optional
#'@return data.frame 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' core_people_get(con = con)
#' }
#'
#'@export
core_people_get <- function(con = NULL,
                            firstName = NULL,
                            lastName = NULL,
                            personDbId = NULL,
                            userID = NULL,
                            commonCropName = NULL,
                            programDbId = NULL,
                            externalReferenceID = NULL,
                            externalReferenceId = NULL,
                            externalReferenceSource = NULL,
                            page = 0,
                            pageSize = 1000) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	queryParams <- list()
	if (!is.null(firstName) && (firstName != "")){
		queryParams[["firstName"]] = firstName
	}
	if (!is.null(lastName) && (lastName != "")){
		queryParams[["lastName"]] = lastName
	}
	if (!is.null(personDbId) && (personDbId != "")){
		queryParams[["personDbId"]] = personDbId
	}
	if (!is.null(userID) && (userID != "")){
		queryParams[["userID"]] = userID
	}
	if (!is.null(commonCropName) && (commonCropName != "")){
		queryParams[["commonCropName"]] = commonCropName
	}
	if (!is.null(programDbId) && (programDbId != "")){
		queryParams[["programDbId"]] = programDbId
	}
	if (!is.null(externalReferenceID) && (externalReferenceID != "")){
		queryParams[["externalReferenceID"]] = externalReferenceID
	}
	if (!is.null(externalReferenceId) && (externalReferenceId != "")){
		queryParams[["externalReferenceId"]] = externalReferenceId
	}
	if (!is.null(externalReferenceSource) && (externalReferenceSource != "")){
		queryParams[["externalReferenceSource"]] = externalReferenceSource
	}
	if (!is.null(page) && (page != "")){
		queryParams[["page"]] = page
	}
	if (!is.null(pageSize) && (pageSize != "")){
		queryParams[["pageSize"]] = pageSize
	}
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "people")

	try({
		## Make the call and receive the response
		resp <- httr::GET(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			query = queryParams
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			if (out$metadata$pagination$totalCount > 0) {
				out$data <-res$result$data
			} else if ("searchResultsDbId" %in% names(res$result)){
				out$data <- res$result
			} else {
				out$data <- data.frame()
			}
		} else {
			message(paste0("The GET people call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "core_people_get")
	return(out)
}