#' GET references
#' @param con list;  required
#' @param referenceDbId string;  optional
#' @param referenceSetDbId string;  optional
#' @param accession string;  optional
#' @param md5checksum string;  optional
#' @param isDerived boolean;  optional
#' @param minLength integer;  optional
#' @param maxLength integer;  optional
#' @param commonCropName string;  optional
#' @param programDbId string;  optional
#' @param trialDbId string;  optional
#' @param studyDbId string;  optional
#' @param externalReferenceId string;  optional
#' @param externalReferenceSource string;  optional
#' @param page integer;  optional
#' @param pageSize integer;  optional
#'@return data.frame 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' genotyping_references_get(con = con)
#' }
#'
#'@export
genotyping_references_get <- function(con = NULL,
                                      referenceDbId = NULL,
                                      referenceSetDbId = NULL,
                                      accession = NULL,
                                      md5checksum = NULL,
                                      isDerived = NA,
                                      minLength = NULL,
                                      maxLength = NULL,
                                      commonCropName = NULL,
                                      programDbId = NULL,
                                      trialDbId = NULL,
                                      studyDbId = NULL,
                                      externalReferenceId = NULL,
                                      externalReferenceSource = NULL,
                                      page = 0,
                                      pageSize = 1000) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	queryParams <- list()
	if (!is.null(referenceDbId) && (referenceDbId != "")){
		queryParams[["referenceDbId"]] = referenceDbId
	}
	if (!is.null(referenceSetDbId) && (referenceSetDbId != "")){
		queryParams[["referenceSetDbId"]] = referenceSetDbId
	}
	if (!is.null(accession) && (accession != "")){
		queryParams[["accession"]] = accession
	}
	if (!is.null(md5checksum) && (md5checksum != "")){
		queryParams[["md5checksum"]] = md5checksum
	}
	if (!is.na(isDerived)){
		queryParams[["isDerived"]] = isDerived
	}
	if (!is.null(minLength) && (minLength != "")){
		queryParams[["minLength"]] = minLength
	}
	if (!is.null(maxLength) && (maxLength != "")){
		queryParams[["maxLength"]] = maxLength
	}
	if (!is.null(commonCropName) && (commonCropName != "")){
		queryParams[["commonCropName"]] = commonCropName
	}
	if (!is.null(programDbId) && (programDbId != "")){
		queryParams[["programDbId"]] = programDbId
	}
	if (!is.null(trialDbId) && (trialDbId != "")){
		queryParams[["trialDbId"]] = trialDbId
	}
	if (!is.null(studyDbId) && (studyDbId != "")){
		queryParams[["studyDbId"]] = studyDbId
	}
	if (!is.null(externalReferenceId) && (externalReferenceId != "")){
		queryParams[["externalReferenceId"]] = externalReferenceId
	}
	if (!is.null(externalReferenceSource) && (externalReferenceSource != "")){
		queryParams[["externalReferenceSource"]] = externalReferenceSource
	}
	if (!is.null(page) && (page != "")){
		queryParams[["page"]] = page
	}
	if (!is.null(pageSize) && (pageSize != "")){
		queryParams[["pageSize"]] = pageSize
	}
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "references")

	try({
		## Make the call and receive the response
		resp <- httr::GET(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			query = queryParams
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			if (out$metadata$pagination$totalCount > 0) {
				out$data <-res$result$data
			} else if ("searchResultsDbId" %in% names(res$result)){
				out$data <- res$result
			} else {
				out$data <- data.frame()
			}
		} else {
			message(paste0("The GET references call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "genotyping_references_get")
	return(out)
}