#' GET studies
#' @param con list;  required
#' @param studyType string;  optional
#' @param locationDbId string;  optional
#' @param seasonDbId string;  optional
#' @param studyCode string;  optional
#' @param studyPUI string;  optional
#' @param observationVariableDbId string;  optional
#' @param active boolean;  optional
#' @param sortBy string;  optional
#' @param sortOrder string;  optional
#' @param commonCropName string;  optional
#' @param programDbId string;  optional
#' @param trialDbId string;  optional
#' @param studyDbId string;  optional
#' @param studyName string;  optional
#' @param germplasmDbId string;  optional
#' @param externalReferenceID string;  optional
#' @param externalReferenceId string;  optional
#' @param externalReferenceSource string;  optional
#' @param page integer;  optional
#' @param pageSize integer;  optional
#'@return data.frame 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' core_studies_get(con = con)
#' }
#'
#'@export
core_studies_get <- function(con = NULL,
                             studyType = NULL,
                             locationDbId = NULL,
                             seasonDbId = NULL,
                             studyCode = NULL,
                             studyPUI = NULL,
                             observationVariableDbId = NULL,
                             active = NA,
                             sortBy = NULL,
                             sortOrder = NULL,
                             commonCropName = NULL,
                             programDbId = NULL,
                             trialDbId = NULL,
                             studyDbId = NULL,
                             studyName = NULL,
                             germplasmDbId = NULL,
                             externalReferenceID = NULL,
                             externalReferenceId = NULL,
                             externalReferenceSource = NULL,
                             page = 0,
                             pageSize = 1000) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	queryParams <- list()
	if (!is.null(studyType) && (studyType != "")){
		queryParams[["studyType"]] = studyType
	}
	if (!is.null(locationDbId) && (locationDbId != "")){
		queryParams[["locationDbId"]] = locationDbId
	}
	if (!is.null(seasonDbId) && (seasonDbId != "")){
		queryParams[["seasonDbId"]] = seasonDbId
	}
	if (!is.null(studyCode) && (studyCode != "")){
		queryParams[["studyCode"]] = studyCode
	}
	if (!is.null(studyPUI) && (studyPUI != "")){
		queryParams[["studyPUI"]] = studyPUI
	}
	if (!is.null(observationVariableDbId) && (observationVariableDbId != "")){
		queryParams[["observationVariableDbId"]] = observationVariableDbId
	}
	if (!is.na(active)){
		queryParams[["active"]] = active
	}
	if (!is.null(sortBy) && (sortBy != "")){
		queryParams[["sortBy"]] = sortBy
	}
	if (!is.null(sortOrder) && (sortOrder != "")){
		queryParams[["sortOrder"]] = sortOrder
	}
	if (!is.null(commonCropName) && (commonCropName != "")){
		queryParams[["commonCropName"]] = commonCropName
	}
	if (!is.null(programDbId) && (programDbId != "")){
		queryParams[["programDbId"]] = programDbId
	}
	if (!is.null(trialDbId) && (trialDbId != "")){
		queryParams[["trialDbId"]] = trialDbId
	}
	if (!is.null(studyDbId) && (studyDbId != "")){
		queryParams[["studyDbId"]] = studyDbId
	}
	if (!is.null(studyName) && (studyName != "")){
		queryParams[["studyName"]] = studyName
	}
	if (!is.null(germplasmDbId) && (germplasmDbId != "")){
		queryParams[["germplasmDbId"]] = germplasmDbId
	}
	if (!is.null(externalReferenceID) && (externalReferenceID != "")){
		queryParams[["externalReferenceID"]] = externalReferenceID
	}
	if (!is.null(externalReferenceId) && (externalReferenceId != "")){
		queryParams[["externalReferenceId"]] = externalReferenceId
	}
	if (!is.null(externalReferenceSource) && (externalReferenceSource != "")){
		queryParams[["externalReferenceSource"]] = externalReferenceSource
	}
	if (!is.null(page) && (page != "")){
		queryParams[["page"]] = page
	}
	if (!is.null(pageSize) && (pageSize != "")){
		queryParams[["pageSize"]] = pageSize
	}
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "studies")

	try({
		## Make the call and receive the response
		resp <- httr::GET(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			query = queryParams
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			if (out$metadata$pagination$totalCount > 0) {
				out$data <-res$result$data
			} else if ("searchResultsDbId" %in% names(res$result)){
				out$data <- res$result
			} else {
				out$data <- data.frame()
			}
		} else {
			message(paste0("The GET studies call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "core_studies_get")
	return(out)
}