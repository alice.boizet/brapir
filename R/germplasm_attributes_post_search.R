#' POST search/attributes
#' @param con list;  required
#' @param attributeCategories array;  optional
#' @param attributeDbIds array;  optional
#' @param attributeNames array;  optional
#' @param attributePUIs array;  optional
#' @param commonCropNames array;  optional
#' @param dataTypes array;  optional
#' @param externalReferenceIDs array;  optional
#' @param externalReferenceIds array;  optional
#' @param externalReferenceSources array;  optional
#' @param germplasmDbIds array;  optional
#' @param germplasmNames array;  optional
#' @param methodDbIds array;  optional
#' @param methodNames array;  optional
#' @param methodPUIs array;  optional
#' @param ontologyDbIds array;  optional
#' @param page integer;  optional
#' @param pageSize integer;  optional
#' @param programDbIds array;  optional
#' @param programNames array;  optional
#' @param scaleDbIds array;  optional
#' @param scaleNames array;  optional
#' @param scalePUIs array;  optional
#' @param studyDbId array;  optional
#' @param studyDbIds array;  optional
#' @param studyNames array;  optional
#' @param traitAttributePUIs array;  optional
#' @param traitAttributes array;  optional
#' @param traitClasses array;  optional
#' @param traitDbIds array;  optional
#' @param traitEntities array;  optional
#' @param traitEntityPUIs array;  optional
#' @param traitNames array;  optional
#' @param traitPUIs array;  optional
#' @param trialDbIds array;  optional
#' @param trialNames array;  optional
#'@return list 
#'
#'
#'@export
germplasm_attributes_post_search <- function(con = NULL,
                                             attributeCategories = c(),
                                             attributeDbIds = c(),
                                             attributeNames = c(),
                                             attributePUIs = c(),
                                             commonCropNames = c(),
                                             dataTypes = c(),
                                             externalReferenceIDs = c(),
                                             externalReferenceIds = c(),
                                             externalReferenceSources = c(),
                                             germplasmDbIds = c(),
                                             germplasmNames = c(),
                                             methodDbIds = c(),
                                             methodNames = c(),
                                             methodPUIs = c(),
                                             ontologyDbIds = c(),
                                             page = 0,
                                             pageSize = 1000,
                                             programDbIds = c(),
                                             programNames = c(),
                                             scaleDbIds = c(),
                                             scaleNames = c(),
                                             scalePUIs = c(),
                                             studyDbId = c(),
                                             studyDbIds = c(),
                                             studyNames = c(),
                                             traitAttributePUIs = c(),
                                             traitAttributes = c(),
                                             traitClasses = c(),
                                             traitDbIds = c(),
                                             traitEntities = c(),
                                             traitEntityPUIs = c(),
                                             traitNames = c(),
                                             traitPUIs = c(),
                                             trialDbIds = c(),
                                             trialNames = c()) {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Construct request body
	body <- list()
	if (length(attributeCategories) > 0) {
		body <- append(body, list(attributeCategories = attributeCategories))
	}
	if (length(attributeDbIds) > 0) {
		body <- append(body, list(attributeDbIds = attributeDbIds))
	}
	if (length(attributeNames) > 0) {
		body <- append(body, list(attributeNames = attributeNames))
	}
	if (length(attributePUIs) > 0) {
		body <- append(body, list(attributePUIs = attributePUIs))
	}
	if (length(commonCropNames) > 0) {
		body <- append(body, list(commonCropNames = commonCropNames))
	}
	if (length(dataTypes) > 0) {
		body <- append(body, list(dataTypes = dataTypes))
	}
	if (length(externalReferenceIDs) > 0) {
		body <- append(body, list(externalReferenceIDs = externalReferenceIDs))
	}
	if (length(externalReferenceIds) > 0) {
		body <- append(body, list(externalReferenceIds = externalReferenceIds))
	}
	if (length(externalReferenceSources) > 0) {
		body <- append(body, list(externalReferenceSources = externalReferenceSources))
	}
	if (length(germplasmDbIds) > 0) {
		body <- append(body, list(germplasmDbIds = germplasmDbIds))
	}
	if (length(germplasmNames) > 0) {
		body <- append(body, list(germplasmNames = germplasmNames))
	}
	if (length(methodDbIds) > 0) {
		body <- append(body, list(methodDbIds = methodDbIds))
	}
	if (length(methodNames) > 0) {
		body <- append(body, list(methodNames = methodNames))
	}
	if (length(methodPUIs) > 0) {
		body <- append(body, list(methodPUIs = methodPUIs))
	}
	if (length(ontologyDbIds) > 0) {
		body <- append(body, list(ontologyDbIds = ontologyDbIds))
	}
	if (!is.na(page)) {
		body <- append(body, list(page = jsonlite::unbox(page)))
	}
	if (!is.na(pageSize)) {
		body <- append(body, list(pageSize = jsonlite::unbox(pageSize)))
	}
	if (length(programDbIds) > 0) {
		body <- append(body, list(programDbIds = programDbIds))
	}
	if (length(programNames) > 0) {
		body <- append(body, list(programNames = programNames))
	}
	if (length(scaleDbIds) > 0) {
		body <- append(body, list(scaleDbIds = scaleDbIds))
	}
	if (length(scaleNames) > 0) {
		body <- append(body, list(scaleNames = scaleNames))
	}
	if (length(scalePUIs) > 0) {
		body <- append(body, list(scalePUIs = scalePUIs))
	}
	if (length(studyDbId) > 0) {
		body <- append(body, list(studyDbId = studyDbId))
	}
	if (length(studyDbIds) > 0) {
		body <- append(body, list(studyDbIds = studyDbIds))
	}
	if (length(studyNames) > 0) {
		body <- append(body, list(studyNames = studyNames))
	}
	if (length(traitAttributePUIs) > 0) {
		body <- append(body, list(traitAttributePUIs = traitAttributePUIs))
	}
	if (length(traitAttributes) > 0) {
		body <- append(body, list(traitAttributes = traitAttributes))
	}
	if (length(traitClasses) > 0) {
		body <- append(body, list(traitClasses = traitClasses))
	}
	if (length(traitDbIds) > 0) {
		body <- append(body, list(traitDbIds = traitDbIds))
	}
	if (length(traitEntities) > 0) {
		body <- append(body, list(traitEntities = traitEntities))
	}
	if (length(traitEntityPUIs) > 0) {
		body <- append(body, list(traitEntityPUIs = traitEntityPUIs))
	}
	if (length(traitNames) > 0) {
		body <- append(body, list(traitNames = traitNames))
	}
	if (length(traitPUIs) > 0) {
		body <- append(body, list(traitPUIs = traitPUIs))
	}
	if (length(trialDbIds) > 0) {
		body <- append(body, list(trialDbIds = trialDbIds))
	}
	if (length(trialNames) > 0) {
		body <- append(body, list(trialNames = trialNames))
	}
	if (length(body) == 0) {
		body <- "{}"
	} else {
		body <- jsonlite::toJSON(body)
	}
	## Obtain the call url
	callurl <- brapi_endpoint_URL(con = con, callPath = "search/attributes")

	try({
		## Make the call and receive the response
		resp <- httr::POST(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			),
			body = body
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			message(paste0("Immediate Response.", "\n"))
			if (out$metadata$pagination$totalCount > 0) {
				out$data <-res$result$data
			} else if ("searchResultsDbId" %in% names(res$result)){
				out$data <- res$result
			} else {
				out$data <- data.frame()
			}
		} else if (httr::status_code(resp) == 202) {
			message(paste0("Saved or Asynchronous Response has provided a searchResultsDbId.", "\n"))
			message(paste0("Use the GET search/attributes/{searchResultsDbId} call to retrieve the paginated output.", "\n"))
			out$data <-res$result
		} else {
			message(paste0("The POST search/attributes call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "germplasm_attributes_post_search")
	return(out)
}