#' GET attributes/{attributeDbId}
#' @param con list;  required
#' @param attributeDbId string;  required
#'@return list 
#'
#' @examples
#' \dontrun{
#' con <- brapi_db()$testserver
#' germplasm_attributes_get_attributeDbId(con = connull)
#' }
#'
#'@export
germplasm_attributes_get_attributeDbId <- function(con = NULL,
                                                   attributeDbId = "") {
	## Check con is not null
	if (is.null(con)) {
		stop("con can not be NULL")
	}
	## Check if BrAPI server can be reached given the connection details
	brapi_checkCon(con = con, verbose = FALSE)
	## Obtain the call url
	callPath <- paste0("attributes/",attributeDbId)
	callurl <- brapi_endpoint_URL(con = con, callPath = callPath)
	try({
		## Make the call and receive the response
		resp <- httr::GET(url = callurl, 
			httr::add_headers( 
				"Authorization" = paste("Bearer", con$token), 
				"Content-Type"= "application/json", 
				"accept"= "*/*"			)
		)
		out <- list(status_code = resp$status_code)
		## Extract the content from the response object in human readable form
		cont <- httr::content(x = resp, as = "text", encoding = "UTF-8")
		## Convert the content object into a data.frame
		res <- jsonlite::fromJSON(cont, flatten = T)
		out$metadata <- res$metadata
		if (out$status_code == 200) {
			if ("data" %in% names(res$result)) {
				out$data <- res$result$data
			} else {
				out$data <- res$result
			}
		} else {
			message(paste0("The GET attributes/{attributeDbId} call resulted in Server status, ", httr::http_status(resp)[["message"]]))
		}
	})
	## Set class of output
	class(out) <- c(class(out), "germplasm_attributes_get_attributeDbId")
	return(out)
}