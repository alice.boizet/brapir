% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/genotyping_variants_get.R
\name{genotyping_variants_get}
\alias{genotyping_variants_get}
\title{GET variants}
\usage{
genotyping_variants_get(
  con = NULL,
  variantDbId = NULL,
  variantSetDbId = NULL,
  referenceDbId = NULL,
  referenceSetDbId = NULL,
  pageToken = NULL,
  page = 0,
  pageSize = 1000,
  externalReferenceId = NULL,
  externalReferenceSource = NULL
)
}
\arguments{
\item{con}{list;  required}

\item{variantDbId}{string;  optional}

\item{variantSetDbId}{string;  optional}

\item{referenceDbId}{string;  optional}

\item{referenceSetDbId}{string;  optional}

\item{pageToken}{string;  optional}

\item{page}{integer;  optional}

\item{pageSize}{integer;  optional}

\item{externalReferenceId}{string;  optional}

\item{externalReferenceSource}{string;  optional}
}
\value{
data.frame
}
\description{
GET variants
}
\examples{
\dontrun{
con <- brapi_db()$testserver
genotyping_variants_get(con = con)
}

}
